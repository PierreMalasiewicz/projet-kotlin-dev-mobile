package com.example.kidisecret.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kidisecret.Objects.Secret
import com.example.kidisecret.R

class SecretListRecyclerAdapter (private val listeSecrets: MutableList<Secret>) : RecyclerView.Adapter<SecretListRecyclerAdapter.ViewHolder>() {

    var onItemClick: ((Secret) -> Unit)? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.secret_listview_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ItemsViewModel = listeSecrets[position]
        holder.secretTitle.text = ItemsViewModel.title
        holder.secretText.text = ItemsViewModel.texte
    }

    override fun getItemCount(): Int {
        return listeSecrets.size
    }


    inner class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val secretTitle: TextView = itemView.findViewById(R.id.secret_list_title)
        val secretText: TextView = itemView.findViewById(R.id.secret_list_text)

        init{
            itemView.setOnClickListener {
                onItemClick?.invoke(listeSecrets[bindingAdapterPosition])
            }
        }
    }


}