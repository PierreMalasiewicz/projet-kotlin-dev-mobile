package com.example.kidisecret.Adapters

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.kidisecret.Objects.Secret
import com.example.kidisecret.R
import java.util.*

class SecretListAdapter(context: Context, resource: Int, objects: Array<Secret>) : ArrayAdapter<Any?>(context, resource, objects)
{

    private val secrets: Array<Secret>

    init {
        secrets = objects as Array<Secret>
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View
    {
        var convertView = convertView

        if (convertView == null)
        {
            convertView = LayoutInflater.from(this.context).inflate(R.layout.secret_listview_item, parent, false)
        }
        val title = convertView!!.findViewById<TextView>(R.id.secret_list_title)
        val text = convertView!!.findViewById<TextView>(R.id.secret_list_text)
        title.text = secrets[position].title
        text.text = secrets[position].texte

        return convertView
    }


}