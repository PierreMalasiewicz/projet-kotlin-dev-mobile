package com.example.kidisecret

import android.app.Application
import com.example.kidisecret.Objects.Secret

class AppClass : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        lateinit var instance: AppClass private set
        var secretList: ArrayList<Secret> = ArrayList<Secret>();
    }


}