package com.example.kidisecret.Activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.example.kidisecret.R
import java.util.*

class SecretEditionActivity : AppCompatActivity() {

    lateinit var titreTv : TextView
    lateinit var texteTv : TextView
    lateinit var id : String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_secret_edition)

        titreTv = this.findViewById(R.id.titreSecret)
        texteTv = this.findViewById(R.id.texteSecret)

        val titreString : String = intent.getStringExtra("secretTitre").toString()
        val texteString : String = intent.getStringExtra("secretTexte").toString()
        id = intent.getStringExtra("secretId").toString()

        if(titreString != "null") titreTv.text = titreString
        if(texteString != "null") texteTv.text = texteString


        if(id == "null")
        {
            id = UUID.randomUUID().toString();
        }

    }


    fun enregistrer(view : View)
    {
        val data = Intent()
        data.putExtra("secretTitre", titreTv.text.toString())
        data.putExtra("secretTexte", texteTv.text.toString())
        data.putExtra("secretId", id)

        setResult(Activity.RESULT_OK, data)
        finish()
    }
}