package com.example.kidisecret.Activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputFilter
import android.view.View
import android.widget.TextView
import com.example.kidisecret.R

class ChangePasswordActivity : AppCompatActivity() {

    lateinit var codeDisplay: TextView;
    var codeText: String = "";

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        codeDisplay = findViewById(R.id.codeTextView);
    }

    fun keyPadHandler(view: View)
    {
        when (view.id) {
            R.id.button0 -> codeText += "0"
            R.id.button1 -> codeText += "1"
            R.id.button2 -> codeText += "2"
            R.id.button3 -> codeText += "3"
            R.id.button4 -> codeText += "4"
            R.id.button5 -> codeText += "5"
            R.id.button6 -> codeText += "6"
            R.id.button7 -> codeText += "7"
            R.id.button8 -> codeText += "8"
            R.id.button9 -> codeText += "9"
            R.id.buttonStar -> codeText += "*"
            R.id.buttonSharp -> codeText += "#"
        }
        codeDisplay.setFilters(arrayOf<InputFilter>(InputFilter.LengthFilter(10)))
        codeDisplay.setText(codeText);
    }

    fun erasePass(view: View)
    {
        codeText = ""
        codeDisplay.setText(codeText)
    }

    fun savePass(view: View)
    {
        val data = Intent()
        data.putExtra("password", codeText)

        setResult(Activity.RESULT_OK, data)
        finish()
    }
}