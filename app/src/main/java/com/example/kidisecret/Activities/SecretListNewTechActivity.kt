package com.example.kidisecret.Activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kidisecret.Adapters.SecretListRecyclerAdapter
import com.example.kidisecret.AppClass
import com.example.kidisecret.Objects.Secret
import com.example.kidisecret.Objects.SwipeGesture
import com.example.kidisecret.R
import com.example.kidisecret.TinyDB

class SecretListNewTechActivity : AppCompatActivity() {

    lateinit var tinyDB : TinyDB


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_secret_list_new_tech)

        tinyDB = TinyDB(applicationContext)
        AppClass.secretList = tinyDB.getListSecret("secretSavedList")

        //setup de base de l'adapter
        val recyclerview = findViewById<RecyclerView>(R.id.secret_recycler)
        recyclerview.layoutManager = LinearLayoutManager(this)
        val adapter = SecretListRecyclerAdapter(AppClass.secretList)
        recyclerview.adapter = adapter

        //Callback qui enregistre le nouveau secret quand on ferme l'activité de création
        var secretLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
               var titre : String? = result.data?.getStringExtra("secretTitre")
               var texte : String? = result.data?.getStringExtra("secretTexte")
               var id : String? = result.data?.getStringExtra("secretId")
               AppClass.secretList.remove(AppClass.secretList.find{it.id == id})
               var nouveauSecret : Secret = Secret(titre!!, texte!!, id!!)
               AppClass.secretList.add(nouveauSecret)
               adapter.notifyDataSetChanged()
               saveListeSecrets()
            }
        }

        //Callback qui enregistre le nouveau mot de passe
        var passwordLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                var password : String? = result.data?.getStringExtra("password")
                tinyDB.putString("savedCode", password);
            }
        }

        //Ouvre l'édition du secret
        adapter.onItemClick = {
                secret -> Log.d("TAG", secret.title)
            val intent = Intent(this, SecretEditionActivity::class.java)
            intent.putExtra("secretTitre", secret.title)
            intent.putExtra("secretTexte", secret.texte)
            intent.putExtra("secretId", secret.id)
            secretLauncher.launch(intent)
        }

        //Gestion swipes pour suppression
        val swipeGesture = object : SwipeGesture()
        {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                when(direction)
                {
                    ItemTouchHelper.RIGHT->{
                        AppClass.secretList.removeAt(viewHolder.bindingAdapterPosition)
                        adapter.notifyDataSetChanged()
                    }
                    ItemTouchHelper.LEFT->{
                        AppClass.secretList.removeAt(viewHolder.bindingAdapterPosition)
                        adapter.notifyDataSetChanged()
                    }
                }
                saveListeSecrets()
            }
        }
        val touchHelper = ItemTouchHelper(swipeGesture)
        touchHelper.attachToRecyclerView(recyclerview)


        //Crée un nouveau secret
        val fabNouveau: View = findViewById(R.id.ajoutSecretFab)
        fabNouveau.setOnClickListener { view ->
            val intent = Intent(this, SecretEditionActivity::class.java)
            secretLauncher.launch(intent)
        }

        //Crée un nouveau secret
        val fabPass: View = findViewById(R.id.changePasswordFab)
        fabPass.setOnClickListener { view ->
            val intent = Intent(this, ChangePasswordActivity::class.java)
            passwordLauncher.launch(intent)
        }

    }


    fun saveListeSecrets()
    {
        tinyDB.putListSecret("secretSavedList", AppClass.secretList)
    }

    override fun onDestroy() {
        tinyDB.putListSecret("secretSavedList", AppClass.secretList)
        super.onDestroy()
    }

}