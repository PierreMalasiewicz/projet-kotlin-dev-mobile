package com.example.kidisecret.Activities

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.kidisecret.R
import com.example.kidisecret.TinyDB


class MainActivity : AppCompatActivity() {

    lateinit var codeDisplay: TextView;
    var codeText: String = "";
    var savedCode : String = "";
    var numberOfTries : Int = 2;

    lateinit var tinyDB : TinyDB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        codeDisplay = findViewById(R.id.codeTextView);

        tinyDB = TinyDB(applicationContext)

        savedCode = tinyDB.getString("savedCode")

        if(savedCode.isNullOrBlank())
        {
            val dialog = AlertDialog.Builder(this)
                .setMessage("Aucun mot de passe enregistré, choisis un mot de passe!")
                .setPositiveButton(android.R.string.yes) { dialog, which -> }
                .show()
            dialog.findViewById<TextView>(android.R.id.message).textSize = 40f
            this.findViewById<TextView>(R.id.LoginButton).setText("Enregistrer mon mot de passe")
        }
    }

    fun keyPadHandler(view: View)
    {
        when (view.id) {
            R.id.button0 -> codeText += "0"
            R.id.button1 -> codeText += "1"
            R.id.button2 -> codeText += "2"
            R.id.button3 -> codeText += "3"
            R.id.button4 -> codeText += "4"
            R.id.button5 -> codeText += "5"
            R.id.button6 -> codeText += "6"
            R.id.button7 -> codeText += "7"
            R.id.button8 -> codeText += "8"
            R.id.button9 -> codeText += "9"
            R.id.buttonStar -> codeText += "*"
            R.id.buttonSharp -> codeText += "#"
        }
        codeDisplay.setFilters(arrayOf<InputFilter>(LengthFilter(10)))
        codeDisplay.setText(codeText);
    }

    fun openKidiSecret(view: View)
    {
        if(savedCode.isNullOrBlank()) {
            savedCode = codeText;
            tinyDB.putString("savedCode", savedCode);
            val dialog = AlertDialog.Builder(this)
                .setMessage("Mot de passe enregistré !")
                .setPositiveButton(android.R.string.yes) { dialog, which -> }
                .show()
            dialog.findViewById<TextView>(android.R.id.message).textSize = 40f
            this.findViewById<TextView>(R.id.LoginButton).setText("Ouvrir ma boite à secrets")
        }
        else
        {
            savedCode = tinyDB.getString("savedCode")
            if(codeText == savedCode && numberOfTries>0)
            {
                val intent = Intent(this, SecretListNewTechActivity::class.java);
                startActivity(intent);
            }
            else if(numberOfTries<1)
            {
                val dialogmessage : String = "Ton Kidisecret™ est bloqué!"
                val dialog = AlertDialog.Builder(this)
                    .setMessage(dialogmessage)
                    .setPositiveButton(android.R.string.yes) { dialog, which -> }
                    .show()
                dialog.findViewById<TextView>(android.R.id.message).textSize = 40f
            }
            else
            {
                val dialog = AlertDialog.Builder(this)
                    .setMessage("Tu as entré le mauvais mot de passe. Il te reste $numberOfTries éssais.")
                    .setPositiveButton(android.R.string.yes) { dialog, which -> }
                    .show()
                dialog.findViewById<TextView>(android.R.id.message).textSize = 40f
                numberOfTries--
            }
        }
    }

    fun erasePass(view: View)
    {
        codeText = ""
        codeDisplay.setText(codeText)
    }

}