# Projet Kotlin Dev Mobile


Ce projet a été créé dans le cadre du cours de Dev Mobile 

L'application est une boite à secrets inspirés des jouets pour enfants "Kidisecrets"<br>
L'application est volontairement "Moche" pour se moquer du jouet

Mon objectif principal était de transférer mes connaissances en android Java vers Kotlin en apprenant les nouvelles techniques et technologies existantes


# Utilisation

Entrer un premier mot de passe

Bouton coeur : Créer un nouveau secret (BIEN APPUYER SUR LE BOUTON "ENREGISTRER" SUR LA PAGE DU SECRET)<br>
Bouton verrou : Modifier le mot de passe

L'application se verrouille après 3 mauvais éssais de mot de passe (relancer l'application permet de retourner à 0 essais)

